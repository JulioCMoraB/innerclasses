/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package innerclasses;

/**
 *
 * @author julio_000
 */
class Interna //Inner Class
{
    public void imprimir(){//metodo dentro de la clase interna
        System.out.println("Esta es la primera clase interna");
        
    }
}
class Interna2
{
    private int x = 10;
    
//Method Local Inner Classes
    public void createLocalInnerClass()//metodo que crea la clase local interna
    {
        int y = 15;
        final int suma =x+y;
        class LocalInner//clase local dentro del metodo
        {
            public void accessOuter() //metodo dentro de la clase local 
            {
                System.out.println(x);
                System.out.println(y); 
                System.out.println(suma);
            }
        }
        
        LocalInner li = new LocalInner();//instanciacion de la clase para poder acceder desde fuera
        li.accessOuter();//invocacion metodo de la clase
    }
    
}
abstract class Anonima{//clase anonima usando abstract
abstract void accionAnonima();

}
class Interna3
{
static int a=30;
static int b=20;
static int multi=a*b;
  static class Anidada{  //clase statica anidada
   void multiplicacion(){System.out.println("La multiplicacion es:"+multi);} //metodo de la clase anidada
  }  
}
public class InnerClasses {
    
    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        // TODO code application logic here
        Interna inte=new Interna();//instanciacion de la clase inner
        inte.imprimir();
        Interna2 interna2=new Interna2();//instaciacion de la clase method-local inner
        interna2.createLocalInnerClass();//llamado al metodo local que crea la clase interna
        Anonima anonim= new Anonima() {
            @Override
            void accionAnonima() {//sobrescribe el metodo de la clase anonima
                System.out.println("Esta es la clase anonima"); 
            }
        };
        anonim.accionAnonima();//invocacion del metodo de la clase anonima en la clase main
        Interna3.Anidada ani=new Interna3.Anidada();//instanciacion de la clases anidada de la Clase Interna3
        ani.multiplicacion();//llamado al metodo de la clase anidada
    
}

}
